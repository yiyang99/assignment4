import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname

def drop_table():
    db, username, password, hostname = get_db_creds()
    cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    cur = cnx.cursor()
    cur.execute("DROP TABLE movie")

def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_movie = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year VARCHAR(255), title VARCHAR(255), director VARCHAR(255), actor VARCHAR(255), release_date VARCHAR(255), rating FLOAT, PRIMARY KEY(id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_movie)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movie(year, title, director, actor, release_date, rating) values ('2019', 'Frozen 2', 'Chris Buck', 'Elsa', '22 Nov 2019', 5)")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movie")
    entries = [dict(movie_id=row[0], movie_year=row[1], movie_title=row[2], movie_director=row[3], movie_actor=row[4], movie_release_date = row[5], movie_ranking = row[6]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request. Add Movie.")
    print(request.form['year'])
    print(request.form['title'])
    print(request.form['director'])
    print(request.form['actor'])
    print(request.form['release_date'])
    print(request.form['rating'])

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    message = []
    try:
        cur = cnx.cursor()
        cur.execute(
            "INSERT INTO movie (year, title, director, actor, release_date, rating) values ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', '" + rating + "')")
        m = "Movie " + title + " successfully inserted"
        message.append(m)
    except Exception as exp:
        m = "Movie " + title + " could not be inserted - " + str(exp)
        message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request. Update movie.")
    print(request.form['year'])
    print(request.form['title'])
    print(request.form['director'])
    print(request.form['actor'])
    print(request.form['release_date'])
    print(request.form['rating'])

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    message = []
    try:
        cur = cnx.cursor()
        cur.execute(
            "UPDATE movie SET year = '" + year + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "' WHERE title = '" + title + "'")
        m = "Movie " + title + " successfully updated"
        message.append(m)
    except Exception as exp:
        m = "Movie " + title + " could not be updated - " + str(exp)
        message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request. Delete Movie.")
    print(request.form['delete_title'])

    delete_title = request.form['delete_title'].lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    message = []
    try:
        cur = cnx.cursor()
        cur.execute("SELECT title FROM movie WHERE title = '" + delete_title + "'")
        entries = [dict(movie_title=row[0]) for row in cur.fetchall()]
        print(entries)
        if len(entries) == 0:
            m = "Movie " + delete_title + " does not exist"
        else:
            cur = cnx.cursor()
            cur.execute(
                "DELETE FROM movie WHERE title = '" + delete_title + "'")
            m = "Movie " + delete_title + " successfully deleted"
        message.append(m)
    except Exception as exp:
        m = "Movie " + delete_title + " could not be deleted - " + str(exp)
        message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request. Search Movie.")
    print(request.args['search_actor'])

    search_actor = request.args['search_actor'].lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute(
        "SELECT * FROM movie WHERE actor = '" + search_actor + "'")
    entries = [dict(movie_title=row[2], movie_year=row[1], movie_actor=row[4]) for row in cur.fetchall()]
    message = []
    if len(entries) == 0:
        m = "No movies found for actor" + search_actor
        message.append(m)
    else:
        for entry in entries:
            m = entry['movie_title'] + ", " + entry['movie_year'] + ", " + entry['movie_actor']
            message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request. Find highest rating.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute(
        "SELECT * FROM movie WHERE rating >= ALL(SELECT rating FROM movie)")
    entries = [dict(movie_title=row[2], movie_year=row[1], movie_actor=row[4], movie_director=row[3], movie_rating=row[6]) for row in cur.fetchall()]
    message = []
    for entry in entries:
        m = entry['movie_title'] + ", " + entry['movie_year'] + ", " + entry['movie_actor'] + ", " + entry['movie_director'] + ", " + str(entry['movie_rating'])
        message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request. Find lowest rating.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute(
        "SELECT * FROM movie WHERE rating <= ALL(SELECT rating FROM movie)")
    entries = [dict(movie_title=row[2], movie_year=row[1], movie_actor=row[4], movie_director=row[3], movie_rating=row[6]) for row in cur.fetchall()]
    message = []
    for entry in entries:
        m = entry['movie_title'] + ", " + entry['movie_year'] + ", " + entry['movie_actor'] + ", " + entry[
            'movie_director'] + ", " + str(entry['movie_rating'])
        message.append(m)
    cnx.commit()
    return render_template('index.html', message=message)


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')

